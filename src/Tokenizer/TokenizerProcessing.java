package Tokenizer;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class TokenizerProcessing {

	private int wordCount;
	private int ngramCount;
	private String result = "";
	private String result2 = "";
	ArrayList<String> list = new ArrayList<String>();
	ArrayList<String> list2 = new ArrayList<String>();

	public TokenizerProcessing() {

	}

	public String wordTokenizer(String sentence) {

		wordCount = 0;
		StringTokenizer st = new StringTokenizer(sentence, " ");

		while (st.hasMoreTokens()) {
			String token = (String) st.nextToken();
			wordCount++;
			list.add(token);

		}
		for (int i = 0; i < list.size(); i++) {
			result = result + (i + 1) + ") " + list.get(i) + "\n";
		}
		result = "|    Word Tokenizing Output    |" + "\n" + "Sentence: "
				+ sentence + "\n" + "Contains: " + wordCount + " tokens" + "\n"
				+ result;
		return result;

	}

	public String ngramTokenizer(String sentence, int n) {
		StringTokenizer st = new StringTokenizer(sentence, " ");

		while (st.hasMoreTokens()) {
			String token = (String) st.nextToken();
			wordCount++;
			list2.add(token);

		}
		for (int i = 0; i < list2.size(); i++) {
			result2 = result2 + list2.get(i);
		}
		int count = result2.length();
		ngramCount = count - (n - 1);
		String title = "|   N-gram Tokenizing Output   |" + "\n" + "Sentence: "
				+ sentence + "\n" + "n = " + n + "\n" + "Contains: "
				+ ngramCount + " tokens\n";
		String gram = "";
		String lastResult;
		for (int i = 0; i < count - (n - 1); i++) {
			gram = gram + (i + 1) + ") " + result2.substring(i, i + n) + "\n";
		}
		lastResult = title + gram;
		return lastResult;

	}
}
