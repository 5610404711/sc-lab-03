package Tokenizer;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class TokenizerFrame extends JFrame {
	String result;

	public TokenizerFrame() {
		createFrame();
	}

	private void createFrame() {
		showProjectName = new JLabel("Output");
		showResults = new JTextArea("");
		endButton = new JButton("end program");
		setLayout(new BorderLayout());
		add(showProjectName, BorderLayout.NORTH);
		add(showResults, BorderLayout.CENTER);
		add(endButton, BorderLayout.SOUTH);
		showResult();

	}

	public void setProjectName(String s) {
		showProjectName.setText(s);
	}

	public void setResult(String str) {
		this.str = str;
		showResults.setText(str + "\n");

	}

	public void showResult() {
		showResults.getText();
		add(showResults, BorderLayout.CENTER);

	}

	public void extendResult(String str) {
		this.str = this.str + "\n" + str;
		showResults.setText(this.str);
	}

	public void setListener(ActionListener list) {
		endButton.addActionListener(list);
	}

	private JLabel showProjectName;
	private JTextArea showResults;
	private JButton endButton;
	private String str;
}
