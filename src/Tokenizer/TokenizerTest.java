package Tokenizer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class TokenizerTest {
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		new TokenizerTest();

	}

	public TokenizerTest() {

		setTestCase();
	}

	public void setTestCase() {

		String str1 = JOptionPane
				.showInputDialog("Please enter the sentence: ");
		JPanel panel = new JPanel();
		JRadioButton bt1 = new JRadioButton("Word Tokenizing");
		JRadioButton bt2 = new JRadioButton("N-gram Tokenizing");
		panel.add(bt1);
		panel.add(bt2);
		JOptionPane.showOptionDialog(null, panel, "Processing Selection",
				JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, null, null);
		if (bt1.isSelected()) {
			TokenizerProcessing test1 = new TokenizerProcessing();
			String result = test1.wordTokenizer(str1);
			frame = new TokenizerFrame();
			frame.setResult(result);
			frame.pack();
			frame.setVisible(true);
			frame.setSize(400, 400);
			list = new ListenerMgr();
			frame.setListener(list);
		} else if (bt2.isSelected()) {
			String count = JOptionPane.showInputDialog("Please Enter n: ");
			int n = Integer.parseInt(count);
			TokenizerProcessing test1 = new TokenizerProcessing();
			String result2 = test1.ngramTokenizer(str1, n);
			frame = new TokenizerFrame();
			frame.setResult(result2);
			frame.pack();
			frame.setVisible(true);
			frame.setSize(400, 400);
			list = new ListenerMgr();
			frame.setListener(list);

		}

	}

	ActionListener list;
	TokenizerFrame frame;
}
